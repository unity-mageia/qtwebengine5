%define snapshot   0
%define pre        0

%ifarch %{arm}
%define build_docs 0
%else
%define build_docs 1
%endif

%define qtmajor 5
%define qtminor 11
%define qtsubminor 0

# NEON support on ARM (detected at runtime) - disable this if you are hitting
# FTBFS due to e.g. GCC bug https://bugzilla.redhat.com/show_bug.cgi?id=1282495
%global arm_neon 1

%define rel 1

%define major_private       1
%define qtversion           %{qtmajor}.%{qtminor}.%{qtsubminor}

%define libwebengine        %mklibname qt5webengine %qtmajor
%define libwebenginewidgets %mklibname qt5webenginewidgets %qtmajor
%define libwebenginecore    %mklibname qt5webenginecore %qtmajor
%define libwebengine_d      %mklibname qt5webengine -d

%if %snapshot
%define qttarballdir        qtwebengine-everywhere-src-%{qtversion}-%pre
%else
%define qttarballdir        qtwebengine-everywhere-src-%{qtversion}
%endif

# exclude plugins (all architectures) and libv8.so (i686, it's static everywhere else)
%global __provides_exclude  ^lib.*plugin\\.so.*|libv8\\.so$
# exclude libv8.so and devel(libv8) (i686, it's static everywhere else)
%global __requires_exclude  ^(libv8\\.so|devel\\(libv8.*)$

Name:           qtwebengine5
Version:        %{qtversion}
Release:        %mkrel %{rel}
Summary:        Integrates chromium's fast moving web capabilities into Qt
Group:          Development/KDE and Qt
URL:            http://www.qt.io
ExcludeArch:    armv5tl
# See LICENSE.GPL LICENSE.LGPL LGPL_EXCEPTION.txt, for details
# See also http://qt-project.org/doc/qt-5.0/qtdoc/licensing.html
# The other licenses are from Chromium and the code it bundles
License: (LGPLv2 with exceptions or GPLv3 with exceptions) and BSD and LGPLv2+ and ASL 2.0 and IJG and MIT and GPLv2+ and ISC and OpenSSL and (MPLv1.1 or GPLv2 or LGPLv2)
# cleaned tarball with patent-encumbered codecs removed from the bundled FFmpeg
# wget http://download.qt.io/official_releases/qt/5.11/5.11.0/submodules/qtwebengine-everywhere-src-5.11.0.tar.xz
# ./clean_qtwebengine.sh 5.11.0
Source0: qtwebengine-everywhere-src-%{version}-clean.tar.xz
# cleanup scripts used above
Source1: clean_qtwebengine.sh
Source2: clean_ffmpeg.sh
Source3: get_free_ffmpeg_source_files.py
# macros
Source10: macros.qt5-qtwebengine
# some tweaks to linux.pri (system yasm, link libpci, run unbundling script)
Patch0:  qtwebengine-everywhere-src-5.10.0-linux-pri.patch
# quick hack to avoid checking for the nonexistent icudtl.dat and silence the
# resulting warnings - not upstreamable as is because it removes the fallback
# mechanism for the ICU data directory (which is not used in our builds because
# we use the system ICU, which embeds the data statically) completely
Patch1:  qtwebengine-opensource-src-5.11.0-no-icudtl-dat.patch
# fix extractCFlag to also look in QMAKE_CFLAGS_RELEASE, needed to detect the
# ARM flags with our %%qmake_qt5 macro, including for the next patch
Patch2:  qtwebengine-opensource-src-5.9.0-fix-extractcflag.patch
# disable NEON vector instructions on ARM where the NEON code FTBFS due to
# GCC bug https://bugzilla.redhat.com/show_bug.cgi?id=1282495
Patch3:  qtwebengine-opensource-src-5.9.0-no-neon.patch
# use the system NSPR prtime (based on Debian patch)
# We already depend on NSPR, so it is useless to copy these functions here.
# Debian uses this just fine, and I don't see relevant modifications either.
Patch4:  qtwebengine-everywhere-src-5.11.0-system-nspr-prtime.patch
# fix missing ARM -mfpu setting
Patch9:  qtwebengine-opensource-src-5.9.2-arm-fpu-fix.patch
# remove Android dependencies from openmax_dl ARM NEON detection (detect.c)
Patch10: qtwebengine-opensource-src-5.9.0-openmax-dl-neon.patch
# webrtc: enable the CPU feature detection for ARM Linux also for Chromium
Patch12: qtwebengine-opensource-src-5.9.0-webrtc-neon-detect.patch
# Force verbose output from the GN bootstrap process
Patch21: qtwebengine-everywhere-src-5.10.0-gn-bootstrap-verbose.patch
## Upstream patches:

BuildRequires:  qtbase5-devel >= %{version}
BuildRequires:	ffmpeg-devel
BuildRequires:  pkgconfig(Qt5Quick) >= %{version}
BuildRequires:  pkgconfig(Qt5WebChannel) >= %{version}
BuildRequires:  pkgconfig(Qt5QuickWidgets)
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(flac)
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(harfbuzz)
BuildRequires:  pkgconfig(icu-i18n)
BuildRequires:  pkgconfig(icu-uc)
BuildRequires:  pkgconfig(lcms2)
BuildRequires:  pkgconfig(libavcodec)
BuildRequires:  pkgconfig(libavformat)
BuildRequires:  pkgconfig(libavutil)
BuildRequires:  pkgconfig(libcap)
BuildRequires:  pkgconfig(libdrm)
BuildRequires:  pkgconfig(libevent)
BuildRequires:  pkgconfig(libpci)
BuildRequires:  pkgconfig(libpng)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libsrtp)
BuildRequires:  pkgconfig(libwebp) >= 0.6.0
BuildRequires:  pkgconfig(libwebpdemux)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(libxslt)
BuildRequires:  pkgconfig(minizip)
BuildRequires:  pkgconfig(nss)
BuildRequires:  pkgconfig(opus)
BuildRequires:  pkgconfig(protobuf)
BuildRequires:  pkgconfig(re2)
BuildRequires:  pkgconfig(speex)
BuildRequires:  pkgconfig(vpx) >= 1.7.0
BuildRequires:  pkgconfig(xcomposite)
BuildRequires:  pkgconfig(xcursor)
BuildRequires:  pkgconfig(xi)
BuildRequires:  pkgconfig(xrandr)
BuildRequires:  pkgconfig(xscrnsaver)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  pkgconfig(jsoncpp)
BuildRequires:  pkgconfig(snappy)
BuildRequires:  git
BuildRequires:  gperf
BuildRequires:  bison
BuildRequires:  flex
%ifarch %{ix86} x86_64
BuildRequires:  yasm
%endif
BuildRequires:  re2c
BuildRequires:  ninja

#Branch Patchs

#Trunk Patchs

#Patch from Mageia

%description
The Qt WebEngine module provides the WebEngineView API which allows QML
applications to render regions of dynamic web content. A WebEngineView
component may share the screen with other QML components or encompass
the full screen as specified within the QML application.

It allows an application to load pages into the WebEngineView, either by
URL or with an HTML string, and navigate within session history. By
default, links to different pages load within the same WebEngineView, but
web sites may request them to be opened as a new tab, window or dialog.

%files
%{_qt5_translationdir}/qtwebengine_locales
%{_qt5_qml}/QtWebEngine/
%{_qt5_prefix}/resources/*.pak
%{_qt5_bindir}/qwebengine_convert_dict
%ifarch %{ix86}
%{_libdir}/qtwebengine
%endif
%{_qt5_prefix}/libexec/QtWebEngineProcess

#------------------------------------------------------------------------------

%if %{build_docs}
%package        doc
Summary:        QtWebEngine%{qtmajor} APIs and tools docs
Group:          Documentation
BuildArch:      noarch
BuildRequires:  qttools5
# This one is required to build QCH-format documentation
# for APIs and tools in this package set
BuildRequires:  qttools5-assistant
Recommends:     qttools5-assistant

%description    doc
Documentation for APIs and tools in QtWebEngine5 package for use with
Qt Assistant.

%files doc
%{_qt5_docdir}/qtwebengine.qch
%{_qt5_docdir}/qtwebengine/
%endif

#------------------------------------------------------------------------------

%package -n     %{libwebengine}
Summary:        Qt%{qtmajor} WebEngine Component Library
Group:          System/Libraries

%description -n %{libwebengine}
Qt%{qtmajor} WebEngine Component Library.

%files -n %{libwebengine}
%{_qt5_libdir}/libQt5WebEngine.so.%{qtmajor}{,*}

#------------------------------------------------------------------------------

%package -n     %{libwebenginecore}
Summary:        Qt%{qtmajor} WebEngineCore Component Library
Group:          System/Libraries
Requires:       %{name} = %{version}

%description -n %{libwebenginecore}
Qt%{qtmajor} WebEngineCore Component Library.

%files -n %{libwebenginecore}
%{_qt5_libdir}/libQt5WebEngineCore.so.%{qtmajor}{,*}

#------------------------------------------------------------------------------

%package -n     %{libwebenginewidgets}
Summary:        Qt%{qtmajor} WebEngineWidgets Component Library
Group:          System/Libraries

%description -n %{libwebenginewidgets}
Qt%{qtmajor} WebEngineWidgets Component Library.

%files -n %{libwebenginewidgets}
%{_qt5_libdir}/libQt5WebEngineWidgets.so.%{qtmajor}{,*}

#------------------------------------------------------------------------------

%package -n     %{libwebengine_d}
Summary:        Devel files needed to build apps based on %{name}
Group:          Development/KDE and Qt
Requires:       %{name} = %{version}
Requires:       %{libwebengine} = %{version}
Requires:       %{libwebenginecore} = %{version}
Requires:       %{libwebenginewidgets} = %{version}
Provides:       libwebengine5-devel = %{version}
Provides:       webengine5-devel = %{version}

%description -n %{libwebengine_d}
Devel files needed to build apps based on %{name}.

%files -n %{libwebengine_d}
%{_qt5_libdir}/*.so
%{_qt5_libdir}/*.prl
%{_qt5_libdir}/pkgconfig/*.pc
%{_qt5_includedir}/QtWebEngine/
%{_qt5_includedir}/QtWebEngineCore/
%{_qt5_includedir}/QtWebEngineWidgets/
%{_qt5_libdir}/cmake/Qt5WebEngine/
%{_qt5_libdir}/cmake/Qt5WebEngineCore/
%{_qt5_libdir}/cmake/Qt5WebEngineWidgets/
%{_qt5_datadir}/mkspecs/modules/*.pri
%{_qt5_examplesdir}/webenginewidgets/
%{_qt5_examplesdir}/webengine/

#------------------------------------------------------------------------------

%prep
%setup -q -n %{qttarballdir}

%patch0 -p1 -b .linux-pri
%patch1 -p1 -b .no-icudtl-dat
%patch2 -p1 -b .fix-extractcflag
%if !0%{?arm_neon}
%patch3 -p1 -b .no-neon
%endif
%patch4 -p1 -b .system-nspr-prtime
%patch9 -p1 -b .arm-fpu-fix
%patch10 -p1 -b .openmax-dl-neon
%patch21 -p1 -b .gn-bootstrap-verbose

# fix // in #include in content/renderer/gpu to avoid debugedit failure
sed -i -e 's!gpu//!gpu/!g' \
  src/3rdparty/chromium/content/renderer/gpu/compositor_forwarding_message_filter.cc
# and another one in 2 files in WebRTC
sed -i -e 's!audio_processing//!audio_processing/!g' \
  src/3rdparty/chromium/third_party/webrtc/modules/audio_processing/utility/ooura_fft.cc \
  src/3rdparty/chromium/third_party/webrtc/modules/audio_processing/utility/ooura_fft_sse2.cc
# remove ./ from #line commands in ANGLE to avoid debugedit failure (?)
sed -i -e 's!\./!!g' \
  src/3rdparty/chromium/third_party/angle/src/compiler/preprocessor/Tokenizer.cpp \
  src/3rdparty/chromium/third_party/angle/src/compiler/translator/glslang_lex.cpp
# delete all "toolprefix = " lines from build/toolchain/linux/BUILD.gn, as we
# never cross-compile in native Fedora RPMs, fixes ARM and aarch64 FTBFS
sed -i -e '/toolprefix = /d' -e 's/\${toolprefix}//g' \
  src/3rdparty/chromium/build/toolchain/linux/BUILD.gn

# http://bugzilla.redhat.com/1337585
# can't just delete, but we'll overwrite with system headers to be on the safe side
cp -bv /usr/include/re2/*.h src/3rdparty/chromium/third_party/re2/src/re2/

# Add back
%if 0
# most arches run out of memory with full debuginfo, so use -g1 on non-x86_64
sed -i -e 's/=-g$/=-g1/g' src/core/gyp_run.pro
%endif

# generate qtwebengine-3rdparty.qdoc, it is missing from the tarball
pushd src/3rdparty
python chromium/tools/licenses.py \
  --file-template ../../tools/about_credits.tmpl \
  --entry-template ../../tools/about_credits_entry.tmpl \
  credits >../webengine/doc/src/qtwebengine-3rdparty.qdoc
popd

# copy the Chromium license so it is installed with the appropriate name
cp -p src/3rdparty/chromium/LICENSE LICENSE.Chromium

%build
export STRIP=strip
export NINJAFLAGS="-v %{_smp_mflags}"
export NINJA_PATH=%{_bindir}/ninja
export CXXFLAGS="%{optflags} -fno-delete-null-pointer-checks"
%ifnarch x86_64
# most arches run out of memory with full debuginfo, so use -g1 on non-x86_64
export CXXFLAGS=`echo "$CXXFLAGS" | sed -e 's/ -g / -g1 /g'`
%endif

%qmake_qt5 QMAKE_EXTRA_ARGS+="-system-webengine-icu"
%make_build

%if %{build_docs}
%__make docs
%endif

%install
%make_install INSTALL_ROOT=%{buildroot}

%if %{build_docs}
%make_install install_docs INSTALL_ROOT=%{buildroot}
%endif

# .la and .a files, die, die, die.
find %{buildroot} -name '*.la' -delete
find %{buildroot} -name '*.a' -delete

## .prl/.la file love
# nuke .prl reference(s) to %%buildroot, excessive (.la-like) libs
pushd %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
  sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
  if [ -f "$(basename ${prl_file} .prl).so" ]; then
    rm -fv "$(basename ${prl_file} .prl).la"
    sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
  fi
done
popd

# adjust cmake dep(s) to allow for using the same Qt5 that was used to build it
# only if older... (I suppose what we really want is the lesser of %%version, %%_qt5_version)
#sed -i -e "s|%{version} \${_Qt5WebEngine|%{_qt5_version} \${_Qt5WebEngine|" \
#  %{buildroot}%{_qt5_libdir}/cmake/Qt5WebEngine*/Qt5WebEngine*Config.cmake

%filetriggerin -- %{_datadir}/myspell
while read filename ; do
  case "$filename" in
    *.dic)
      bdicname=%{_qtwebengine_dictionaries_dir}/`basename -s .dic "$filename"`.bdic
      %{_qt5_bindir}/qwebengine_convert_dict "$filename" "$bdicname" &> /dev/null || :
      ;;
  esac
done

